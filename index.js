const stdin = process.stdin;
let cents = 0;

stdin.setRawMode(true);
stdin.resume();
stdin.setEncoding('utf8');

function Update(input) {
	cents += input;
	const total = (cents / 100).toFixed(2);

	if (input > 0) console.log(`${total} \x1b[32m(+${input}c)\x1b[0m`);
	else console.log(`${total} \x1b[31m(${input}c)\x1b[0m`);
};

stdin.on('data', (key) => {
	switch(key) {
		case '\x03':
			process.exit();
			break;
		case '1':
			Update(50);
			break;
		case '2':
			Update(-50);
			break;
		case 'q':
			Update(20);
			break;
		case 'w':
			Update(-20);
			break;
		case 'a':
			Update(10);
			break;
		case 's':
			Update(-10);
			break;
		case 'z':
			Update(5);
			break;
		case 'x':
			Update(-5);
			break;
		case '0':
			console.log(':: Cents reset to zero.')
			cents = 0;
			break;
	};
});

